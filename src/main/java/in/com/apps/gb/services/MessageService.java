package in.com.apps.gb.services;

import java.util.List;

import in.com.apps.gb.model.Message;

public interface MessageService {
	public Message save(Message message);
	public List<Message> getAllMessage();
}
