package in.com.apps.gb.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.com.apps.gb.model.Message;
import in.com.apps.gb.repository.MessageRepository;

@Service
public class MessageServiceImpl implements MessageService {

	private final MessageRepository messageRepository;
	
	@Autowired
	public MessageServiceImpl(MessageRepository messageRepository){
	this.messageRepository=messageRepository;	
	}

	@Override
	@Transactional
	public Message save(Message message) {
		// TODO Auto-generated method stub
		System.out.println("Name: " + message.getName());
		System.out.println("Email: " + message.getEmail());
		System.out.println("Comment: " + message.getComment());
		return messageRepository.save(message);
	}

	@Override
	@Transactional
	public List<Message> getAllMessage() {
		return messageRepository.findAll();
	}

}
