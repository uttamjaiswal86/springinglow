package in.com.apps.gb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import in.com.apps.gb.model.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {

}
