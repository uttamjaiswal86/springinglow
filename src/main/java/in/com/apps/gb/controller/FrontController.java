package in.com.apps.gb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import in.com.apps.gb.model.Message;
import in.com.apps.gb.services.MessageService;

@Controller
@EnableWebMvc
@RequestMapping(value="/")
public class FrontController {
	
	@Autowired
	MessageService messageService;
	
	@RequestMapping(method=RequestMethod.GET)
	public String welcome(Model model){
		model.addAttribute("msgList", messageService.getAllMessage());
		return "index";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String saveMessage(Message msg,Model model){
		messageService.save(msg);
		model.addAttribute("msgList", messageService.getAllMessage());
		return "index";
	}

}
