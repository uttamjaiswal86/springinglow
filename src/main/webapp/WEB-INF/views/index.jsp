<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sform" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset="UTF-8">
  <title>bootstrap guestbook</title>
  <script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
	<meta name="viewport" content="width=device-width">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css'>   
</head>
<body>
<div class="page-header">
  <div class="container">
   <h1 class="text-center">Guest Book Form</h1>
  </div>
</div>
 
<div class="container">
	<div class="row">
	<div class="col-sm-6 col-md-offset-3">
		<sform:form method="POST" commandName="/">
			<div class="form-group">
			<label>Name:</label>
			<input type=text name="name" class="form-control">
			</div>
			<div class="form-group">
			<label>Email Address: </label>
			<input type="text" name="email" class="form-control">
			</div>
			<div class="form-group">
			<label>Comments: </label>
			<textarea name="comment" rows="5" cols="60" class="form-control"></textarea>
			</div>
		<input type="submit" value="Submit" class="btn btn-success">
		</sform:form>
	</div>
	</div>
	<hr><br/><br/>
  <div class="row">
    <div class="col-sm-6 col-md-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          Leave A Comment
        </div>
        <div class="panel-body">
           <ul>
           <c:forEach items="${msgList}" var="message" >
          <li id="message_<c:out value="message.name"/>">
            <div class="message"><c:out value="${message.comment}" /></div>
            <div>
              <span class="email"><c:out value="${message.email}" /></span>
            </div>
          </li>
        </c:forEach>
           </ul>
        </div>
      </div>
    </div>
  </div>
</div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
</body>
</html>